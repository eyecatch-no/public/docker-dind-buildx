# oci-builder
Images for building OCI images with either Buildah or Docker.

[![pipeline status](https://gitlab.com/eyecatch-no/public/oci-builder/badges/main/pipeline.svg)](https://gitlab.com/eyecatch-no/public/oci-builder/-/commits/main)

It also has versions with other services set up in addition to DinD and BuildX, the tags should be self-explanatory.

Docker Hub:  
https://hub.docker.com/repository/docker/eyecatch/oci-builder
